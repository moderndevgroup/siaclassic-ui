import React from 'react'
import PropTypes from 'prop-types'

const BalanceInfo = ({
  synced,
  confirmedbalance,
  unconfirmedbalance,
  siaclassicfundbalance,
  siaclassiccoinclaimbalance
}) => (
  <div className='balance-info'>
    <span>Confirmed Balance: {confirmedbalance} SAC </span>
    <span>Unconfirmed Delta: {unconfirmedbalance} SAC </span>
    {siaclassicfundbalance !== '0' ? (
      <span> SiaClassicfund Balance: {siaclassicfundbalance} SF </span>
    ) : null}
    {siaclassiccoinclaimbalance !== '0' ? (
      <span> SiaClassiccoin Claim Balance: {siaclassiccoinclaimbalance} SAC </span>
    ) : null}
    {!synced ? (
      <span
        style={{ marginRight: '40px', color: 'rgb(255, 93, 93)' }}
        className='fa fa-exclamation-triangle'
      >
        Your wallet is not synced, balances are not final.
      </span>
    ) : null}
  </div>
)
BalanceInfo.propTypes = {
  synced: PropTypes.bool.isRequired,
  confirmedbalance: PropTypes.string.isRequired,
  unconfirmedbalance: PropTypes.string.isRequired,
  siaclassicfundbalance: PropTypes.string.isRequired,
  siaclassiccoinclaimbalance: PropTypes.string.isRequired
}
export default BalanceInfo
