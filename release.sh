#!/bin/bash

# error output terminates this script
set -e

# This script creates a SiaClassic-UI release for all 3 platforms: osx (darwin),
# linux, and windows. It takes 5 arguments, the first two arguments are the
# private and public key used to sign the release archives. The last three
# arguments are semver strings, the first of which being the ui version, second
# being the SiaClassic version, and third being the electron version.

if [[ -z $1 || -z $2 ]]; then
	echo "Usage: $0 privatekey publickey uiversion siaclassicversion electronversion"
	exit 1
fi

# ensure we have a clean node_modules
rm -rf ./node_modules
npm install

# build the UI's js
rm -rf ./dist
npm run build

uiVersion=${3:-v1.3.4.1}
siaclassicVersion=${4:-v1.3.4.1}
electronVersion=${5:-v2.0.2}

# fourth argument is the public key file path.
keyFile=`readlink -f $1`
pubkeyFile=`readlink -f $2`


electronOSX="https://github.com/electron/electron/releases/download/${electronVersion}/electron-${electronVersion}-darwin-x64.zip"
electronLinux="https://github.com/electron/electron/releases/download/${electronVersion}/electron-${electronVersion}-linux-x64.zip"
electronWindows="https://github.com/electron/electron/releases/download/${electronVersion}/electron-${electronVersion}-win32-x64.zip"

siaclassicOSX="/media/vangyangpao/Sata/go/src/gitlab.com/moderndevgroup/SiaClassic/release/SiaClassic-${siaclassicVersion}-darwin-amd64.zip"
siaclassicLinux="/media/vangyangpao/Sata/go/src/gitlab.com/moderndevgroup/SiaClassic/release/SiaClassic-${siaclassicVersion}-linux-amd64.zip"
siaclassicWindows="/media/vangyangpao/Sata/go/src/gitlab.com/moderndevgroup/SiaClassic/release/SiaClassic-${siaclassicVersion}-windows-amd64.zip"

rm -rf release/
mkdir -p release/{osx,linux,win32}

# package copies all the required javascript, html, and assets into an electron package.
package() {
	src=$1
	dest=$2
	cp -r ${src}/{plugins,assets,css,dist,app.html,app.js,package.json,js} $dest
}

buildOSX() {
	cd release/osx
	wget $electronOSX 
	unzip ./electron*
	mv Electron.app SiaClassic-UI.app
	mv SiaClassic-UI.app/Contents/MacOS/Electron SiaClassic-UI.app/Contents/MacOS/SiaClassic-UI
	# NOTE: this only works with GNU sed, other platforms (like OSX) may fail here
	sed -i 's/>Electron</>SiaClassic-UI</' SiaClassic-UI.app/Contents/Info.plist 
	sed -i 's/>'"${electronVersion:1}"'</>'"${siaclassicVersion:1}"'</' SiaClassic-UI.app/Contents/Info.plist
	sed -i 's/>com.github.electron\</>com.moderndevgroup.siaclassicui</' SiaClassic-UI.app/Contents/Info.plist
	sed -i 's/>electron.icns</>icon.icns</' SiaClassic-UI.app/Contents/Info.plist
	cp ../../assets/icon.icns SiaClassic-UI.app/Contents/Resources/
	rm -r SiaClassic-UI.app/Contents/Resources/default_app.asar
	mkdir SiaClassic-UI.app/Contents/Resources/app
	(
		cd SiaClassic-UI.app/Contents/Resources/app
		cp $siaclassicOSX .
		unzip ./SiaClassic-*
		rm ./SiaClassic*.zip
		mv ./SiaClassic-* ./SiaClassic
	)
	package "../../" "SiaClassic-UI.app/Contents/Resources/app"
	rm -r electron*.zip
	cp ../../LICENSE .
}

buildLinux() {
	cd release/linux
	wget $electronLinux
	unzip ./electron*
	mv electron SiaClassic-UI
	rm -r resources/default_app.asar
	mkdir resources/app
	(
		cd resources/app
		cp $siaclassicLinux .
		unzip ./SiaClassic-*
		rm ./SiaClassic*.zip
		mv ./SiaClassic-* ./SiaClassic
	)
	package "../../" "resources/app"
	rm -r electron*.zip
	cp ../../LICENSE .
}

buildWindows() {
	cd release/win32
	wget $electronWindows
	unzip ./electron*
	mv electron.exe SiaClassic-UI.exe
	wget https://github.com/electron/rcedit/releases/download/v0.1.0/rcedit.exe
	wine rcedit.exe SiaClassic-UI.exe --set-icon '../../assets/icon.ico'
	rm -f rcedit.exe
	rm resources/default_app.asar
	mkdir resources/app
	(
		cd resources/app
		cp $siaclassicWindows .
		unzip ./SiaClassic-*
		rm ./SiaClassic*.zip
		mv ./SiaClassic-* ./SiaClassic
	)
	package "../../" "resources/app"
	rm -r electron*.zip
	cp ../../LICENSE .
}

# make osx release
( buildOSX )

# make linux release
( buildLinux )

# make windows release
( buildWindows )

# make signed zip archives for each release
for os in win32 linux osx; do 
	(
		cd release/${os}
		zip -r ../SiaClassic-UI-${uiVersion}-${os}-x64.zip .
		cd ..
		openssl dgst -sha256 -sign $keyFile -out SiaClassic-UI-${uiVersion}-${os}-x64.zip.sig SiaClassic-UI-${uiVersion}-${os}-x64.zip
		if [[ -n $pubkeyFile ]]; then
			openssl dgst -sha256 -verify $pubkeyFile -signature SiaClassic-UI-${uiVersion}-${os}-x64.zip.sig SiaClassic-UI-${uiVersion}-${os}-x64.zip
		fi
		rm -rf release/${os}
	)
done

