import { Menu } from 'electron'

export default function (window) {
  // Template for SiaClassic-UI tray menu.
  const menutemplate = [
    {
      label: 'Show SiaClassic',
      click: () => window.show()
    },
    { type: 'separator' },
    {
      label: 'Hide SiaClassic',
      click: () => window.hide()
    },
    { type: 'separator' },
    {
      label: 'Quit SiaClassic',
      click: () => {
        window.webContents.send('quit')
      }
    }
  ]

  return Menu.buildFromTemplate(menutemplate)
}
